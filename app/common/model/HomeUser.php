<?php

namespace app\common\model;

use think\Model;

class HomeUser extends Model
{
    /**
     * 获取用户列表
     * @param $where
     * @param $limit
     * @return array
     */
    public function getUserList($where, $limit)
    {
        try {

            $list = $this->where($where)->order('user_id','desc')->paginate($limit);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0,'success', $list);
    }
}