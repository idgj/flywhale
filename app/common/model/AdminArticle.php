<?php

namespace app\common\model;

use think\Model;

class AdminArticle extends Model
{
    /**
     * 获取文章列表
     * @param $where
     * @param $limit
     * @return array
     */
    public function getArticleList($where, $limit)
    {
        try {

            $list = $this->alias('a')->field('a.*,c.name as cate_name')
                ->leftJoin('admin_cate c', 'a.cate_id = c.cate_id')
                ->where($where)->order('article_id','desc')->where('is_delete', 1)->paginate($limit);
        } catch (\Exception $e) {

            return dataReturn(-1, $e->getMessage());
        }


        return dataReturn(0, 'success', $list);
    }
}