<?php

declare (strict_types = 1);

namespace app\common\validate;

use think\Validate;

class AdminFormValidate extends Validate
{
    protected $rule = [
        'title|表单名称' => 'require|chs',
        'table|表名' => 'require|regex: /^[a-z][a-z0-9\_]+$/'
    ];
}